package com.admin.spring.Controller;

import com.admin.spring.Entity.enrollmentsystem.Course;
import com.admin.spring.Entity.enrollmentsystem.Student;
import com.admin.spring.Exception.ResourceNotFoundException;
import com.admin.spring.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
//@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping("/student")
    public String addStudent(@RequestBody Student student){
        return studentService.saveStudent(student);

    }




    @GetMapping("/student/{id}")
    public Student getStudentById(
            @PathVariable(value = "id") int id)
    {
        return studentService.getStudent(id);
    }

    @PutMapping("/students/update/{id}")
    public String updateStudent(@PathVariable(value = "id") int id,
                                                   @RequestBody Student students) throws ResourceNotFoundException {

            Student student = studentService.getStudent(id);

            student.setName(students.getName());
            student.setPassword(students.getPassword());
            student.setCourse(students.getCourse());

            return studentService.saveStudent(student);


    }

    @DeleteMapping("/student/delete/{id}")
    public String deleteStudent(@PathVariable(value = "id") int id) {
        Student student = studentService.getStudent(id);
        return studentService.deleteStudent(student);

    }
}
