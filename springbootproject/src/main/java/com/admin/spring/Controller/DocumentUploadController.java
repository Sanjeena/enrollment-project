package com.admin.spring.Controller;

import com.admin.spring.Service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class DocumentUploadController {

    @Autowired
    private DocumentService documentService;

    @PostMapping("/upload/file")
    public void uploadFile(@RequestParam("files") MultipartFile[]  files){
       for(MultipartFile file: files){
             documentService.saveFile(file);
       }

    }


}
