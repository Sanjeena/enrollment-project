package com.admin.spring.Controller;

import com.admin.spring.Entity.enrollmentsystem.User;
import com.admin.spring.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {
    @Autowired
    private UserService userService;

    @PostMapping("/user/add")
    public String addUser(@RequestBody User user){
        return userService.saveUser(user);

    }
}
