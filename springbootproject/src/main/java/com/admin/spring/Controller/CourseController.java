package com.admin.spring.Controller;

import com.admin.spring.Entity.enrollmentsystem.Course;
import com.admin.spring.Service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CourseController {

    @Autowired
    private CourseService courseService;


    @PostMapping("/course")
    public String addCourse(@RequestBody Course course) {
        return courseService.saveCourse(course);

    }

    @GetMapping("/allcourse")
    public List<Course> allCourse() {
        return courseService.getAllCourse();

    }

    @GetMapping("/course/{id}")
    public Course getCourseById(@PathVariable(value = "id") int id) {
        return courseService.getCourse(id);
    }

    @DeleteMapping("/course/delete/{id}")
    public String deleteCourse(@PathVariable(value = "id") int id) {
        Course course = courseService.getCourse(id);

        return courseService.deleteCourse(course);

    }

    @PutMapping("/course/update/{id}")
    public String updateCourse(@PathVariable(value = "id") int id,
                               @RequestBody Course courses) {
        Course course = courseService.getCourse(id);
        course.setCourseName(courses.getCourseName());
        course.setSupervisorName(courses.getSupervisorName());


        return courseService.saveCourse(course);
    }

    @GetMapping("/registerCourse")
    public String register(Model model) {
        model.addAttribute("course", new Course());

        return "info";
    }

    @PostMapping("/savecourse")
    public String courseSave(@ModelAttribute Course course, Model model){
       courseService.saveCourse(course);
        model.addAttribute("courses",courseService.getAllCourse());
        return "home";
    }
}

