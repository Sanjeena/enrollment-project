package com.admin.spring.Controller;

import com.admin.spring.Service.SwitchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class SwitchController {

    @Autowired
    private SwitchService switchService;

    @PostMapping("/upload/excelfile")
    public void uploadExcelFile(@RequestParam("excelfile") MultipartFile[]  files){
        for(MultipartFile file: files){
            switchService.save(file);
        }

    }



}
