package com.admin.spring.Controller;

import com.admin.spring.Entity.enrollmentsystem.TextFile;
import com.admin.spring.Service.TextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;


@RestController
public class TextFileController {
    @Autowired
    private TextService textService;

    @PostMapping("/upload/textfile")
    public void uploadTextFile(){

        String file = "C:\\Users\\user\\IdeaProjects\\springbootproject\\springbootproject\\src\\main\\resources\\data.txt";
          textService.save(file);

    }


    @GetMapping("/textfile")
    public List<TextFile> getTextData(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy)
    {
        return textService.getAllFileData(pageNo, pageSize, sortBy);


    }

}
