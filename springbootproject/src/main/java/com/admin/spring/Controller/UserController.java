package com.admin.spring.Controller;

import com.admin.spring.Entity.enrollmentsystem.AuthRequest;
import com.admin.spring.Service.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @Autowired
    private AuthenticationManager authenticationManager;


        @Autowired
        private JwtUtil jwtUtil;

    @PostMapping("/authenticate")
    public String generateToken(@RequestBody AuthRequest authRequest) throws Exception{
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        }catch (Exception ex){
            throw new Exception("invalid username or password");
        }
     return jwtUtil.generateToken(authRequest.getUsername());
    }




}
