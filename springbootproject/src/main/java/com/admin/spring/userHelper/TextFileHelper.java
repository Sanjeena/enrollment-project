package com.admin.spring.userHelper;

import com.admin.spring.Entity.enrollmentsystem.TextFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TextFileHelper {

    public static List<TextFile> txtToDB(String file) {
//      public static void txtToDB(String file) {
        List<TextFile> textFileList = new ArrayList<TextFile>();
        try {

            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(new FileInputStream(file), "UTF-8"));
            String thisline;

            while ((thisline = reader.readLine()) != null) {

                String str[] = thisline.split(",");

                int stringlen = str.length;

                int counter = 0;

                while (stringlen != 0) {


                    TextFile textFile = new TextFile();
                    for (int i = 0; i < 3; i++) {


                        if (str[counter].contains("CARD ENTERED")) {
                            String check[] = str[counter].split(":");
                            textFile.setCardEnter(check[1]);
                            stringlen--;
                            counter++;


                        } else if (str[counter].contains("TIME")) {
                            String check[] = str[counter].split(" ");
                            textFile.setTime(check[1]);
                            stringlen--;
                            counter++;

                        } else if (str[counter].contains("CRD NO")) {
                            String check[] = str[counter].split(":");
                            textFile.setCrdNo(check[1]);
                            stringlen--;
                            counter++;
                        }


                    }


                    textFileList.add(textFile);
                }


            }
            return textFileList;

        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());

        }
    }
}

