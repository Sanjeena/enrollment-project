package com.admin.spring.userHelper;

import com.admin.spring.Entity.enrollmentsystem.SwitchData;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SwitchHelpers {

    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static String[] HEADERS = {"tandate", "TanType", "advice_type", "Device_type", "termid", "card_no", "rrno", "authid", "cycode", "txnamt", "response_code", "mefield", "recon_number"};
   


    public static boolean hasExcelFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static List<SwitchData> excelToSwitch(InputStream file) {
        List<SwitchData> switchDataList=new ArrayList<SwitchData>();
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheet1 = workbook.getSheetAt(0);

            int rowCount = sheet1.getPhysicalNumberOfRows();
            System.out.println(rowCount);
            for (int i = 1; i < rowCount; i++) {
                SwitchData switchData1 = new SwitchData();
                XSSFRow row = sheet1.getRow(i);
                int cellCount = row.getPhysicalNumberOfCells();
                for (int j = 0; j <cellCount; j++) {
                    XSSFCell cell = row.getCell(j);
                    switch (j) {
                        case 0:
                            switchData1.setTranDate(cell.getDateCellValue());
                            break;

                        case 1:
                            switchData1.setTanType(cell.getStringCellValue());
                            break;

                        case 2:
                            switchData1.setAdviceType(cell.getStringCellValue());
                            break;

                        case 3:
                            switchData1.setDeviceType(cell.getStringCellValue());
                            break;

                        case 4:
                            cell.setCellType(CellType.STRING);
                            switchData1.setTermID(cell.getStringCellValue());
                            break;

                        case 5:
                            cell.setCellType(CellType.STRING);
                            switchData1.setCardNO(cell.getStringCellValue());
                            break;

                        case 6:
                            cell.setCellType(CellType.STRING);
                            switchData1.setRrNO(cell.getStringCellValue());
                            break;

                        case 7:
                            switchData1.setAuthID(cell.getNumericCellValue());
                            break;

                        case 8:
                            switchData1.setCyCODE(cell.getStringCellValue());
                            break;

                        case 9:
                            switchData1.setTxnAMT(cell.getNumericCellValue());
                            break;

                        case 10:
                            switchData1.setResponseCODE(cell.getNumericCellValue());
                            break;


                        default:
                            break;
                    }

                }

                switchDataList.add(switchData1);
            }
            workbook.close();

            return switchDataList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());

        }
    }
}
