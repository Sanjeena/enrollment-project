package com.admin.spring.Dao;

import com.admin.spring.Entity.enrollmentsystem.TextFile;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TextFileDao extends PagingAndSortingRepository<TextFile,Integer> {

}
