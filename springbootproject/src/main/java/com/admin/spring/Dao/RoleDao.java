package com.admin.spring.Dao;

import com.admin.spring.Entity.enrollmentsystem.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role,Integer> {
}
