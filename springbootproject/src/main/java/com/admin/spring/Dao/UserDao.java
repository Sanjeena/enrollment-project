package com.admin.spring.Dao;

import com.admin.spring.Entity.enrollmentsystem.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User,Integer>{
    User findByUsername(String username);
}
