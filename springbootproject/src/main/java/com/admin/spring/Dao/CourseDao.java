package com.admin.spring.Dao;

import com.admin.spring.Entity.enrollmentsystem.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseDao extends JpaRepository<Course,Integer> {

}
