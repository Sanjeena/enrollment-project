package com.admin.spring.Dao;

import com.admin.spring.Entity.enrollmentsystem.SwitchData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SwitchDao extends JpaRepository<SwitchData,Integer> {
}
