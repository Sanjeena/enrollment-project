package com.admin.spring.Dao;

import com.admin.spring.Entity.enrollmentsystem.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentDao extends JpaRepository<Document,Integer> {

}
