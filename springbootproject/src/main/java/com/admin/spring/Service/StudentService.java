package com.admin.spring.Service;

import com.admin.spring.Dao.StudentDao;
import com.admin.spring.Entity.enrollmentsystem.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

@Autowired
    StudentDao studentDao;

 public Student getStudent(int id){
    return studentDao.findById(id).get();

 }

 public String saveStudent(Student student){
     try{
         studentDao.save(student);
         return "successfully saved";
     }catch(Exception ex){
         ex.printStackTrace();
     }
     return "error occured";

 }

 public String deleteStudent(Student student){
     try {
         studentDao.delete(student);
         return "delete successfully";
     }catch(Exception ex){
         ex.printStackTrace();
     }
     return "error encountered";
 }
}
