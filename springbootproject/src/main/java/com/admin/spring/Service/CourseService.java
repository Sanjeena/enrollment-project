package com.admin.spring.Service;

import com.admin.spring.Dao.CourseDao;
import com.admin.spring.Entity.enrollmentsystem.Course;
import com.admin.spring.Exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseDao courseDao;

    public Course getCourse(int id) {
        try {
            return courseDao.findById(id).get();
        } catch (ResourceNotFoundException exc) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Course Not Found", exc);
        }
    }

    public List<Course> getAllCourse(){
     try {
         return courseDao.findAll();
     }catch (ResourceNotFoundException ex){
         throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Course Not Found",ex);
     }



    }

    public String saveCourse(Course course) {
        try {
            courseDao.save(course);
            return "successfully saved";
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "error occured";
    }

    public String deleteCourse(Course course) {
        try {
            courseDao.delete(course);
            return "delete successfully";
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "delete cannot be done";
    }
}
