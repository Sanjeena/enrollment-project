package com.admin.spring.Service;

import com.admin.spring.Dao.SwitchDao;
import com.admin.spring.Entity.enrollmentsystem.SwitchData;

import com.admin.spring.userHelper.SwitchHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class SwitchService {
    @Autowired
    private SwitchDao switchDao;

    public void save(MultipartFile file) {
        try {
            List<SwitchData> switchDataList = SwitchHelpers.excelToSwitch(file.getInputStream());
            switchDao.saveAll(switchDataList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public List<SwitchData> getAllSwitchData() {
        return switchDao.findAll();
    }
}
