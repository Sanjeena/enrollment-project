package com.admin.spring.Service;

import com.admin.spring.Dao.DocumentDao;
import com.admin.spring.Entity.enrollmentsystem.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class DocumentService {
    @Autowired
    private DocumentDao documentdao;

    public String saveFile(MultipartFile file){
        String docname=file.getOriginalFilename();
        try{
            Document doc=new Document(docname,file.getContentType(),file.getBytes());
             documentdao.save(doc);
             return "Successfully uploaded";

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

}
