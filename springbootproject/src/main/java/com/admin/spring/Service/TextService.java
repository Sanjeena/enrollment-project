package com.admin.spring.Service;

import com.admin.spring.Dao.TextFileDao;
import com.admin.spring.Entity.enrollmentsystem.TextFile;
import com.admin.spring.userHelper.TextFileHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TextService {


    @Autowired
    private TextFileDao textFileDao;

    public void save(String file) {

        List<TextFile> textFileList = TextFileHelper.txtToDB(file);
        "hello".indexOf("el");
        textFileDao.saveAll(textFileList);
    }


    public List<TextFile> getAllFileData(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<TextFile> pagedResult = textFileDao.findAll(paging);
        if (pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<TextFile>();
        }

    }
}
