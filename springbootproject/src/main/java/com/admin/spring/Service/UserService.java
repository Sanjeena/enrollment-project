package com.admin.spring.Service;

import com.admin.spring.Dao.UserDao;
import com.admin.spring.Entity.enrollmentsystem.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserDao userDao;


    public String saveUser(User user){
        try{
            userDao.save(user);
            return "successfully saved";
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return "error occured";

    }

}
