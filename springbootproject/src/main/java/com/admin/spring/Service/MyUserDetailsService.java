package com.admin.spring.Service;

import com.admin.spring.Dao.UserDao;
import com.admin.spring.Entity.enrollmentsystem.User;
import com.admin.spring.userHelper.UserHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
           User user=userDao.findByUsername(username);
           if(user==null) {
               throw new UsernameNotFoundException("user not found");
           }
           return new UserHelpers(user);

//        return new User("foo","foo",new ArrayList<>());

    }
}
