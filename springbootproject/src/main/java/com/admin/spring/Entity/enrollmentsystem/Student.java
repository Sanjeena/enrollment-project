package com.admin.spring.Entity.enrollmentsystem;


import com.admin.spring.Entity.enrollmentsystem.Course;

import javax.persistence.*;

@Entity
@Table(name="Student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID", nullable=false)
    private int id;

    @Column (name="NAME")
    private String name;

    @Column (name="PASSWORD")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JoinColumn(name = "COURSE", referencedColumnName = "COURSE_ID", nullable =false)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private Course course;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

}
