package com.admin.spring.Entity.enrollmentsystem;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="TextFile")
public class TextFile implements Serializable {
    private static final long serialVersionUID =1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private int id;

    @Column(name = "CARD_ENTERED", nullable = false, length = 45)
    private String cardEnter;

    @Column(name = "TIME", nullable = false, length = 45)
    private String time;

    @Column(name = "CRD_NO", nullable = false, length = 45)
    private String crdNo;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardEnter() {
        return cardEnter;
    }

    public void setCardEnter(String cardEnter) {
        this.cardEnter = cardEnter;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCrdNo() {
        return crdNo;
    }

    public void setCrdNo(String crdNo) {
        this.crdNo = crdNo;
    }



}
