package com.admin.spring.Entity.enrollmentsystem;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="Switch")
public class SwitchData implements Serializable {

    private static final long serialVersionUID =1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SWITCH_ID", nullable = false)
    private int id;

    @Column(name = "TRAN_DATE", nullable = false, length = 45)
    private Date tranDate;

    @Column(name = "TRAN_TYPE", nullable = false, length = 45)
    private String tanType;

    @Column(name = "ADVICE_TYPE", nullable = false, length = 45)
    private String adviceType;

    @Column(name = "DEVICE_TYPE", nullable = false, length = 45)
    private String deviceType;

    @Column(name = "TERM_ID", nullable = false, length = 45)
    private String termID;

    @Column(name = "CARD_NO", nullable = false, length = 45)
    private String cardNO;

    @Column(name = "RR_NO", nullable = false, length = 45)
    private String rrNO;

    @Column(name = "AUTH_ID", nullable = false)
    private double authID;

    @Column(name = "CY_CODE", nullable = false, length = 45)
    private String cyCODE;

    @Column(name = "TXN_AMT", nullable = false)
    private double txnAMT;

    @Column(name = "RESPONSE_CODE", nullable = false)
    private double responseCODE;

    public String getTermID() {
        return termID;
    }

    public void setTermID(String termID) {
        this.termID = termID;
    }

    public String getCardNO() {
        return cardNO;
    }

    public void setCardNO(String cardNO) {
        this.cardNO = cardNO;
    }

    public String getRrNO() {
        return rrNO;
    }

    public void setRrNO(String rrNO) {
        this.rrNO = rrNO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTranDate() {
        return tranDate;
    }

    public void setTranDate(Date tranDate) {
        this.tranDate = tranDate;
    }

    public String getTanType() {
        return tanType;
    }

    public void setTanType(String tanType) {
        this.tanType = tanType;
    }

    public String getAdviceType() {
        return adviceType;
    }

    public void setAdviceType(String adviceType) {
        this.adviceType = adviceType;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getCyCODE() {
        return cyCODE;
    }

    public void setCyCODE(String cyCODE) {
        this.cyCODE = cyCODE;
    }

    public double getAuthID() {
        return authID;
    }

    public void setAuthID(double authID) {
        this.authID = authID;
    }

    public double getTxnAMT() {
        return txnAMT;
    }

    public void setTxnAMT(double txnAMT) {
        this.txnAMT = txnAMT;
    }

    public double getResponseCODE() {
        return responseCODE;
    }

    public void setResponseCODE(double responseCODE) {
        this.responseCODE = responseCODE;
    }


}
