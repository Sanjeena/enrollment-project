package com.admin.spring.Entity.enrollmentsystem;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.annotation.Generated;
import javax.persistence.*;

@Entity
@Table
@NoArgsConstructor
@Getter
@Setter
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String docName;
    private String docType;

    @Lob
    private byte[] data;


    public Document(String docName, String docType, byte[] data) {
        super();
        this.docName=docName;
        this.docType=docType;
        this.data=data;
    }

}
