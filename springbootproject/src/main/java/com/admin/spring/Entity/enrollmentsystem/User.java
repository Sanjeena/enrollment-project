package com.admin.spring.Entity.enrollmentsystem;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name="User")
public class User {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="USER_ID", nullable=false)
        private Integer id;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        @Column (name="NAME")
        private String username;

        @Column (name="PASSWORD")
        private String password;

        @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
        @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "USER_ID"), inverseJoinColumns = @JoinColumn(name = "role_id",referencedColumnName = "ROLE_ID"))
        private Set<Role> roles=new HashSet<>();

        public User() {
        }

        public Set<Role> getRoles() {
                return roles;
        }

        public void setRoles(Set<Role> roles) {
                this.roles = roles;
        }

        public User(User user) {
                this.username = user.getUsername();
                this.roles = user.getRoles();
                this.id = user.getId();
                this.password = user.getPassword();
        }



        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public String getPassword() {
                return password;
        }

        public void setPassword(String password) {
                this.password = password;
        }


}
