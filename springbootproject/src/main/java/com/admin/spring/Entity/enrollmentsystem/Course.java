package com.admin.spring.Entity.enrollmentsystem;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table (name="COURSE")
public class Course implements Serializable {

    private static final long serialVersionUID =1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COURSE_ID", nullable = false)
    private int id;

    @Basic(optional = true)
    @Column(name = "COURSE_NAME", nullable = false, length = 45)
    private String courseName;

    @Basic(optional = true)
    @Column(name = "SUPERVISOR_NAME", nullable = false, length = 45)
    private String supervisorName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getSupervisorName() {
        return supervisorName;
    }

    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }


}
